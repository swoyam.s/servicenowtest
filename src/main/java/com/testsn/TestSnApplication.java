package com.testsn;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.identityconnectors.common.security.GuardedString;
import org.identityconnectors.framework.api.APIConfiguration;
import org.identityconnectors.framework.api.ConfigurationProperties;
import org.identityconnectors.framework.api.ConnectorFacade;
import org.identityconnectors.framework.api.ConnectorFacadeFactory;
import org.identityconnectors.framework.api.ConnectorInfo;
import org.identityconnectors.framework.api.ConnectorInfoManager;
import org.identityconnectors.framework.api.ConnectorInfoManagerFactory;
import org.identityconnectors.framework.api.ConnectorKey;
import org.identityconnectors.framework.api.RemoteFrameworkConnectionInfo;
import org.identityconnectors.framework.common.objects.ObjectClass;
import org.identityconnectors.framework.common.objects.Uid;
import org.identityconnectors.framework.common.objects.Attribute;
import org.identityconnectors.framework.common.objects.AttributeBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSnApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSnApplication.class, args);
		
		ConnectorInfoManagerFactory fact = ConnectorInfoManagerFactory.getInstance();
		String password = "swoyamss";
		
		RemoteFrameworkConnectionInfo remoteFrameworkConnectionInfo = new RemoteFrameworkConnectionInfo("localhost", 8759,
				new GuardedString(password.toCharArray()));
        ConnectorInfoManager manager = fact.getRemoteManager(remoteFrameworkConnectionInfo);
        ConnectorKey key = new ConnectorKey("net.tirasa.connid.bundles.servicenow", "1.0.0", "net.tirasa.connid.bundles.servicenow.SNConnector");       
        ConnectorInfo info = manager.findConnectorInfo(key);
        APIConfiguration apiConfig = info.createDefaultAPIConfiguration();
        ConfigurationProperties properties = apiConfig.getConfigurationProperties();
        properties.setPropertyValue("username", "admin");
        properties.setPropertyValue("password", new GuardedString("AP1rraNvW%6*".toCharArray()));
        properties.setPropertyValue("baseAddress", "https://dev112873.service-now.com");
        
        ConnectorFacade conn = ConnectorFacadeFactory.getInstance().newInstance(apiConfig);
        conn.validate();
        conn.test();
        Set<Attribute> set = new HashSet<>();
        Map<String, Object> map = new HashMap<>();
        map.put("user_name", "sudipta1234");
        map.put("name", "sudipta nayak");
        map.put("email", "sudipta@gmail.com");
        
        for(Entry<String, Object> obj : map.entrySet())
        {
        	set.add(AttributeBuilder.build(obj.getKey(), obj.getValue()));
        }
//        conn.create(ObjectClass.ACCOUNT, set, null);
        
        conn.update(ObjectClass.ACCOUNT, new Uid("b48761c21b34111024eaece4604bcbc9"), set, null);
        
//        conn.delete(ObjectClass.ACCOUNT, new Uid("b48761c21b34111024eaece4604bcbc9"), null);
        
        conn.search(null, null, null, null);
        
        
	}

}
